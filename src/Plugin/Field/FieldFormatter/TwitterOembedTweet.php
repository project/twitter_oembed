<?php

namespace Drupal\twitter_oembed\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Link;

/**
 * A field formatter for linking and wrapping text.
 *
 * @FieldFormatter(
 *   id = "twitter_oembed_tweet",
 *   label = @Translation("Twitter OEmbed Tweet"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class TwitterOembedTweet extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $output = [];
    $attributes = new Attribute();
    $classes = $this->getSetting('classes');
    if (!empty($classes)) {
      $attributes->addClass($classes);
    }

    $parent = $items->getParent()->getValue();
    foreach ($items as $item) {
      $text = $item->getValue()['value'];
      if ($this->getSetting('linked')) {
        $text = Link::fromTextAndUrl($text, $parent->toUrl())->toString();
      }
      $oEmbed = \Drupal::service('twitter_oembed.embed');
      $data = $oEmbed->getEmbedCode((string)$text);
      $output[] = [
        '#theme' => 'twitter_oembed_tweet',
        '#tweet' => $data,
        '#attributes' => $attributes->toArray(),
        '#attached' => [
          'library' => [
            'twitter_oembed/widget',
          ],
        ],
      ];
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['classes'] = [
      '#title' => $this->t('Classes'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('classes'),
    ];
    return $form;
  }

}
