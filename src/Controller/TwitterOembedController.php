<?php

namespace Drupal\twitter_oembed\Controller;


use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for BigPipe module routes.
 */
class TwitterOembedController extends ControllerBase {

  public function content() {
    $oEmbed = \Drupal::service('twitter_oembed.embed');
    $url = 'https://twitter.com/drupal/status/1575455654613057537';
    $data = $oEmbed->getEmbedCode($url);
    return [
      '#theme' => 'twitter_oembed_tweet',
      '#tweet' => $data,
      '#attached' => [
        'library' => [
          'twitter_oembed/widget',
        ],
      ],
    ];
  }

}
