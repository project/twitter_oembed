<?php

namespace Drupal\twitter_oembed;

use GuzzleHttp\Client;

/**
 * Twitter OEmbed service.
 */
class TwitterOembed {

  private Client $client;

  const TWITTER_OEMBED_ENDPOINT = 'https://publish.twitter.com/oembed';

  public function __construct() {
    $this->client = new Client();
  }

  /**
   * Provides embed code for the URL.
   *
   * @param string $url
   * @return array|mixed|string
   */
  public function getEmbedCode(string $url) {
    $options = [
      'query' => ['url' => $url]
    ];
    $response = $this->client->get(self::TWITTER_OEMBED_ENDPOINT, $options);
    $output = json_decode($response->getBody(), true);
    return $output['html'];
  }
}
